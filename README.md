# test-rails

Basic docker file (to be imported as Image by other Dockerfile) using:
Alpine 3.13
Ruby 3.1.0
Node 14.x

## Intro

With Rails 7 (and probably many previous versions), you need to have both ruby and node ready. This docker image starts from `ruby:3.1.0-alpine3.15` image and add the following packages/gems

## Build packages

- build-base
- curl-dev
- git
- curl
- wget
- shared-mime-info

## Development packages

- postgresql-dev
- yaml-dev
- zlib-dev
- nodejs
- npm
- yarn

## Ruby packages and gems

- tzdata
- rails -v 7.0.1
- rake -v 13.0.6
- bundler -v 2.3.5

The command `RUN gem update --system` is also called to make sure the image has an up to date Ruby on Rails environment.

## A typical .gitlab-ci will starts like this

```
image: 'docker.io/ycrepeau/test-rails:7.0.1amd64'

services:
  - postgres:latest
  - redis:latest

variables:
  POSTGRES_DB: <project_name>_postgres_test # watch config/database.yml
  POSTGRES_USER: postgres
  POSTGRES_PASSWORD: ''
  POSTGRES_HOST_AUTH_METHOD: trust
  RAILS_ENV: test
  DATABASE_URL: 'postgresql://postgres:postgres@postgres:5432/$POSTGRES_DB'

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - app/assets/builds/
    - vendor/bundle/
    - public/
    - node_modules/

stages:         # List of stages for jobs, and their order of execution
  - build
  - test
  - deploy

before_script:
  - bundle config set --local deployment 'true'
  - bundle check || bundle install --jobs $(nproc)

build-job: # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "Building environment..."
    - bundle install -j $(nproc) # Install dependencies into ./vendor/ruby

    # Setting up database
    - bundle exec rake db:create RAILS_ENV=test
    - rails db:migrate
    - rails db:seed

    # Building assets
    - npx esbuild app/javascript/*.* --bundle  --outdir=app/assets/builds
    - npx sass ./app/assets/stylesheets/application.sass.sass ./app/assets/builds/application.css --no-source-map --load-path=node_modules

# ...
```
