FROM ruby:3.1.0-alpine3.15

ENV RAILS_ENV='test'
ENV RAILS_ENV='test'

ENV BUILD_PACKAGES="build-base curl-dev git curl wget shared-mime-info"
ENV DEV_PACKAGES="postgresql-dev yaml-dev zlib-dev nodejs npm"
ENV RUBY_PACKAGES="tzdata"

ENV RAILS_ROOT /var/www/app_name


# install packages
RUN apk update \
  && apk upgrade \
  && apk add --update --no-cache $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES \
  && apk add --no-cache yarn --repository="http://dl-cdn.alpinelinux.org/alpine/edge/community" \
  && yarn -v

# Install bundler
RUN gem update --system
#RUN gem install mimemagic -v 0.4.3
RUN gem install rails -v 7.0.1
RUN gem install rake -v 13.0.6
RUN gem install bundler -v 2.3.5
RUN which rails
RUN which bundler
RUN which rake
RUN which yarn
RUN which npx

# docker tag local-image:tagname new-repo:tagname
# docker push new-repo:tagname
# docker push ycrepeau/test-rails:ycrepeau/test-rails:ycretagname
# docker buildx build --platform linux/amd64,linux/arm64 --push -t <tag_to_push> .